/**
 * @file
 * om_license.modal.js
 */

/**
 * Modal window.
 */
(function ($) {

  /**
   * Attach behaviors.
   */
  Drupal.behaviors.om_license = {
    attach: function(context, settings) {
      this.context = context;
      this.settings = settings;

      this.attachEvents();
    },
    detach: function(context) {
      
    },

    /**
     * Attach om license events.
     */
    attachEvents: function() {
      // Fire modal window on validation.
      if (this.settings.om_license && 
          this.settings.om_license.modal) {

        this.fireModalWindow();
      }
    },

    /**
     * Fires the om license modal window.
     */
    fireModalWindow: function() {
      var basePath = this.settings.basePath;

      $(window).bind('load', function() {
        var url = basePath + 'admin/modules/list/nojs/om_license';
        var link = $('<a></a>').attr('href', url).addClass('ctools-modal-license-modal-style ctools-use-modal-processed').click(Drupal.CTools.Modal.clickAjaxLink);

        Drupal.ajax[url] = new Drupal.ajax(url, link.get(0), {
          url: url,
          event: 'click',
          progress: {
            type: 'throbber'
          }
        });

        link.click();
      });
    }
  },

  /**
   * Modal content.
   */
  Drupal.theme.prototype.license_modal = function () {
    var html = '';
      html += '<div id="ctools-modal">';
      html += '  <div class="ctools-modal-content ctools-modal-license-modal-content">';
      html += '    <span class="popups-close"><a class="close" href="#">' + Drupal.CTools.Modal.currentSettings.closeImage + '</a></span>';
      html += '    <div class="modal-scroll"><div id="modal-content" class="modal-content popups-body"></div></div>';
      html += '  </div>';
      html += '</div>';

    return html;
  }

})(jQuery);
