<?php

/**
 * @file
 * Module file.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function om_views_banner_filter_form_internal_banner_node_form_alter(&$form, &$form_state) {
  $form['#validate'][] = 'om_views_banner_filter_form_internal_banner_node_form_validate';
}

/**
 * Validate callback.
 */
function om_views_banner_filter_form_internal_banner_node_form_validate(&$form, &$form_state) {
  $pathsValue = $form_state['values']['field_display_on_these_paths'];
  foreach ($pathsValue[LANGUAGE_NONE] as $path) {
    if (empty($path['value'])) {
      continue;
    }

    $query = new EntityFieldQuery();
    $query
      ->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'internal_banner')
      ->fieldCondition('field_display_on_these_paths', 'value', $path['value']);

    // Exclude this node from the query if isn't a new node.
    if (!empty($form['#node']->nid)) {
      $query->propertyCondition('nid', $form['#node']->nid, '!=');
    }

    $results = $query->execute();

    if ($results) {
      $node = node_load(key($results['node']));
      form_set_error('field_display_on_these_paths', t('The path %path is already in use on %node', [
        '%path' => $path['value'],
        '%node' => $node->title,
      ]));
    }
  }
}

/**
 * Implements hook_views_api().
 */
function om_views_banner_filter_views_api() {
  return [
    'api' => 3,
    'path' => drupal_get_path('module', 'om_views_banner_filter') . '/includes',
  ];
}

/**
 * Implements hook_views_plugins().
 */
function om_views_banner_filter_views_plugins() {
  $plugins = [
    'argument default' => [
      'banner_url' => [
        'title' => t('Raw URL to Banner'),
        'handler' => 'views_plugin_argument_default_banner_url',
      ],
    ],
  ];
  return $plugins;
}
