
========================================
Online Medical Video Banner - OMV Banner
v0.1
========================================


DEPENDECIES
------------
field_collection
field_group
references
browserclass
mediaelement.js


DESCRIPTION
-----------
Provides text sychronisation with HTML5 video as a responsive video banner. 
It allows users to create customised messages that are synced to user defined edit points within the video.
Click through to destination pages.
Image fallback for mobile devices.


INSTALLING
----------
OMV Banner is intergrated into the OM Base theme by default but needs to be enabled to work.


CONFIGURATION
-------------
1. Go to admin/structure/views
   Enable the Video banner view. This will generate a Video banner block.

2. Go to admin/structure/block
   Find the OMV Banner block and place in the region you'd like it to display

3. A default content page has already been created in the OM base theme. 
   Find the page "OMV banner - demo" and modify the content to your needs.


USEAGE
-------
The video banner content type provides fields for configuring the messages to be displayed over the video banner.
Each slide can be inserted at specific times and can be displayed for a set duration.

The field set (slide) provides
	– Main message
	– Support message
	– Button link destination
	– Button label
	– Cuepoint (the time the slide should be inserted)
	– Duration (how long it will be displayed on screen)

An unlimited amount of slides can be added and they can then be synced to specific time in the video.

There is also an option to enable a timecode overlay to see the videos current time.

The video banner is responisve, so it will scale to fit its container. 
Move it to a full width region for fluid video scaling.

It can be capped to a max height also. 

It has been created to be used a singular instance, but if multiple instances of the banner are required, the view could be modified to generate a new block and use nodeID as a filter to pull in the content needed.



CAVEAT
-------
The ordering of the slides must match the order you intend the insert the slides timings.

eg:	Slide 1 inserted at 15sec
	Slide 2 inserted at 5secs

This example will error as Slide 2 is inserted earlier in time, so it must be ordered first in the list.


DISABLING
----------
If you don't wish to use the video banner, the easiest method is to disable the view.
To remove all omvbanner libraries from the sites build files

1. Open the Gruntfile.js and comment the lines referencing the following files
   – mediaelement-and-player.min.js
   – omvbanner.js
   This will exclude the required libraries from the app.min.js build

2. Open the omtheme.scss
   Comment the omvbanner.scss to exclude required styles from the compiled css





