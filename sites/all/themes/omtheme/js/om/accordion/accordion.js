
/* Implement custom javascript here */

(function ($, Drupal, window, document, undefined) {

	Drupal.behaviors.omaccordion = {
	  attach: function(context, settings) {


	    $( document ).ready(function() {

	    
			/* Accordion */
			if($('.ui-accordion').length > 0){

					var hash = document.URL.substr(document.URL.indexOf('#')+1);

					/* Expands the accoridon panel base on a URL hash  
					Add a numbered anchor to the page URL with the accordion
					and it will expand that panel ID 	eg: site.com/faq#3 */

			    	if(window.location.hash) {
						$(".ui-accordion").accordion({
						 autoHeight: false,
						 navigation: true,
						 active: 1,
						 activate: function( event, ui ) {
								if(!$.isEmptyObject(ui.newHeader.offset())) {
									$('html:not(:animated), body:not(:animated)').animate({ scrollTop: ui.newHeader.offset().top }, 'slow');
								}
							}
						});
						$(".ui-accordion").accordion("option" , "active", parseInt(hash) );
						

					} else{

						//	Scrolls the active accordion panel to the top

				    	$( ".ui-accordion" ).accordion({
							heightStyle: "content",
							active: false,
							activate: function( event, ui ) {
								if(!$.isEmptyObject(ui.newHeader.offset())) {
									$('html:not(:animated), body:not(:animated)').animate({ scrollTop: ui.newHeader.offset().top }, 'slow');
								}
							}
						});
					}
				}



		   });
	    }
	};


})(jQuery, Drupal, this, this.document);

