/* Implement custom javascript here */

(function ($, Drupal, window, document, undefined) {

	Drupal.behaviors.my_custom_behavior = {
	  	attach: function(context, settings) {
			// Convert flexslider slideshow inline images into background images
			$('.front .flexslider .slides > li > img').replaceWith(function(){
			return $('<div>', {
				style: 'background-image: url('+this.src+')',
				class: 'slideshow__image'
			});
		});

		var bannerURL = $('.node .field-name-field-banner-image > img').attr("src");
		$('.banner_image_dynamic').css('background-image', 'url(' + bannerURL + ')');


	    $( document ).ready(function() {

		/* Add active class to parent items of active item  */
		$('#main-menu li').each(function(){
							
					if ($(this).find('.dropdown > li').hasClass('expanded')){
						//$(this).find('.dropdown > li').removeClass('expanded');
						$(this).find('.dropdown > li').removeClass('not-click');
						$(this).find('.dropdown > li').removeClass('has-dropdown');
					}
					if ($(this).find('.dropdown > li > a').hasClass('active')){
						$(this).find('> a').addClass('active');
					
					}
				});
				$('.top-bar .top-bar-section #main-menu li.js-generated').click(function(){
					$('.top-bar .top-bar-section').removeClass('left-100');
				});

			});
		}
	};


})(jQuery, Drupal, this, this.document);



