<?php

/**
 * Implements template_preprocess_html().
 */
function omtheme_preprocess_html(&$variables) {
    drupal_add_css('https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800&display=swap', array('type' => 'external'));
}

/**
 * Implements template_preprocess_page.
 */
function omtheme_preprocess_page(&$variables) {
}

/**
 * Implements template_preprocess_node.
 */
function omtheme_preprocess_node(&$variables) {
}
